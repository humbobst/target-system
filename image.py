import numpy as np
import cv2
import math
import datetime
import os
import vec

def find_coeffs(src_coords, dst_coords):
    a = np.array(src_coords, dtype="float32")
    b = np.array(dst_coords, dtype="float32")
    return cv2.getPerspectiveTransform(a, b)

def increase_brightgray(img, brightness = 50, contrast = 50):
    img = np.int16(img)
    img = img * (contrast/127+1) - contrast + brightness
    img = np.clip(img, 0, 255)
    img = np.uint8(img)
    return img

def increase_brightness(img, value=30):
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    h, s, v = cv2.split(hsv)

    lim = 255 - value
    v[v > lim] = 255
    v[v <= lim] += value

    final_hsv = cv2.merge((h, s, v))
    img = cv2.cvtColor(final_hsv, cv2.COLOR_HSV2BGR)
    return img

class Image:
    def __init__(self, cv2_image):
        self.img = cv2_image
        x, y = cv2_image.shape[:2]
        self.size = (x, y)
    def copy(self):
        return Image(self.img.copy())
    def show(self):
        cv2.imshow('a', self.img)
    def delta(self, img2):
        diff = cv2.absdiff(self.img, img2.img)
        gray = Image(diff).grayscale()
        return Image(increase_brightgray(gray.img, brightness = 75, contrast = 75))
    def grayscale(self):
        return Image(cv2.cvtColor(self.img, cv2.COLOR_BGR2GRAY))
    def resize(self, new_size):
        return Image(cv2.resize(self.img, new_size, interpolation = cv2.INTER_AREA))
    def overlay(self, layer):
        return Image(cv2.addWeighted(self.img, 0.4, layer.img, 0.6, 0))
    def bgr(self):
        return Image(cv2.cvtColor(self.img, cv2.COLOR_GRAY2BGR))
    def save(self, path):
        cv2.imwrite(path, self.img)
    def perspective_transform(self, coeffs, size):
        return Image(cv2.warpPerspective(self.img, coeffs, size))
    def copy(self):
        return Image(self.img.copy())
    def paste(self, other, pos):
        # self must be a BGR (not BGRA) image. other must be BGRA.
        self.img.setflags(write=1)
        img = self.img
        img_overlay = other.img[:,:,0:3]
        alpha_mask = other.img[:, :, 3] / 255.0
        x, y = pos

        # Image ranges
        y1, y2 = max(0, y), min(img.shape[0], y + img_overlay.shape[0])
        x1, x2 = max(0, x), min(img.shape[1], x + img_overlay.shape[1])

        # Overlay ranges
        y1o, y2o = max(0, -y), min(img_overlay.shape[0], img.shape[0] - y)
        x1o, x2o = max(0, -x), min(img_overlay.shape[1], img.shape[1] - x)

        # Exit if nothing to do
        if y1 >= y2 or x1 >= x2 or y1o >= y2o or x1o >= x2o:
            return

        channels = img.shape[2]

        alpha = alpha_mask[y1o:y2o, x1o:x2o]
        alpha_inv = 1.0 - alpha

        for c in range(channels):
            overlay = img_overlay[y1o:y2o, x1o:x2o, c]
            source = img[y1:y2, x1:x2, c]
            img[y1:y2, x1:x2, c] = (alpha * overlay + alpha_inv * source)

    def draw_ring(self, diameter):
        w, h = self.size
        center = (w//2, h//2)
        self.draw_circle(center, diameter, (255, 255, 0), 1)

    def draw_circle(self, center, diameter, rgb, thickness = 2):
        cv2.circle(self.img, center, int(diameter * 0.5), rgb, thickness)

    def scoring_filter(self):
        return self

    def threshold(self, middle):
        _, img = cv2.threshold(self.img, middle, 255, cv2.THRESH_BINARY)
        return Image(img)

    def xor(self, other):
        x = cv2.bitwise_xor(self.img, other.img)
        return Image(x)

def load(path):
    cv2_image = cv2.imread(path, cv2.IMREAD_UNCHANGED)
    return Image(cv2_image)

class DetectedShotRegion:
    def __init__(self, base_image, point, hole_diameter_px, debugInfo):
        cx, cy = point
        w, h = base_image.size
        hole_diameter_px = int(hole_diameter_px)
        image_width = hole_diameter_px * 3
        hw = image_width // 2
        self.x1 = max(0, cx - hw)
        self.y1 = max(0, cy - hw)
        self.x2 = min(w, cx + hw)
        self.y2 = min(h, cy + hw)

        self.image = Image(base_image.img[self.y1:self.y2, self.x1:self.x2])
        self.tvalue = (np.amin(self.image.img) * 2 + np.amax(self.image.img)) // 3
        self.thresh = self.image.threshold(self.tvalue)
        self.hole_diameter_px = hole_diameter_px
        self.point = point
        debugInfo.thresh_image = self.thresh
    def translate_point(self, p):
        x, y = p
        return (x - self.x1, y - self.y1)
    def point_in_region(self, p):
        x, y = p
        return self.x2 > x >= self.x1 and self.y2 > y >= self.y1
    def rank_potential_point(self, p, known_shots):
        # we make a black image, and render a white circle on it at
        # the potential shot location then we render black circles
        # over top of that for the existing shots, potentially
        # blacking out parts of our new shot circle then, we XOR that
        # with our thresholded image. the rank is based on how much white
        # is left in the result (less is better!)

        # this works because we want one where we *do* cover up most
        # of the difference area (which XOR white will blacken),
        # but *don't* reach out into the unchanged areas (which XOR
        # white would brighten)

        mask_image = Image(np.zeros((self.thresh.size[0], self.thresh.size[1], 1), np.uint8))
        # draw proposed shot as white
        mask_image.draw_circle(self.translate_point(p), self.hole_diameter_px, 255, -1)
        # draw existing shots over as black
        for shot in known_shots:
            mask_image.draw_circle(self.translate_point(shot.pixel_pos), self.hole_diameter_px, 0, -1)
        # xor
        xored = mask_image.xor(self.thresh)
        # rank (less is better, so we negate this result)
        return -cv2.countNonZero(xored.img)
    def refine_point(self, known_shots):
        ix, iy = self.point
        rad = self.hole_diameter_px // 2 + 1
        known_shots = [ s for s in known_shots if self.point_in_region(s.pixel_pos) ]
        points = [ (x, y) for x in range(ix - rad, ix + rad) for y in range(iy - rad, iy + rad) ]
        return max(points, key=lambda p: self.rank_potential_point(p, known_shots))

class DebugInfo:
    def __init__(self, prev_image, new_image, diff_image, keypoints):
        self.prev_image = prev_image
        self.new_image = new_image
        self.diff_image = diff_image
        self.keypoints = keypoints
        self.thresh_image = None
    def dump(self, folder, modified_point = None):
        try:
            os.makedirs(folder)
        except FileExistsError:
            # directory already exists
            pass
        self.keypoints_image = Image(cv2.drawKeypoints(self.diff_image.img, self.keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS))
        self.prev_image.save(folder + "/prev_image.png")
        self.new_image.save(folder + "/new_image.png")
        self.diff_image.save(folder + "/diff_image.png")
        self.keypoints_image.save(folder + "/keypoints_image.png")
        if self.thresh_image != None:
            self.thresh_image.save(folder + "/thresh_image.png")
        if modified_point != None:
            old_point, new_point = modified_point
            def draw(i, name):
                i.draw_circle(old_point, 5, (255, 0, 0), 1)
                i.draw_circle(new_point, 15, (0, 255, 255), 1)
                i.save(folder + "/" + name)
            draw(self.diff_image.bgr(), "moved_point_d.png")
            draw(self.new_image.copy(), "moved_point_n.png")

class Detection:
    def __init__(self, point, debugInfo, hole_diameter_px):
        self.point = point
        self.debug = debugInfo
        self.hole_diameter_px = hole_diameter_px
    def refine(self, known_shots):
        region = DetectedShotRegion(self.debug.diff_image, self.point, self.hole_diameter_px, self.debug)
        refined = region.refine_point(known_shots)
        self.point = refined

# Detector parameters have been tuned to produce good results
# with test data from "samples" folder.

# two possible schools of thought here:

# a. stay with default minRepeatability of 2. use a large
# thresholdStep of 10. maxThreshold of 150 or 200. consider
# target1's 352-353 problem a separate issue, solve it by
# either double-checking results with a new picture, or by
# simply ignoring the 1st picture after any detected hit
# (because nobody's shooting that fast in 10m AP or AR)

# b. increase minRepeatability to 3. use a smaller
# thresholdStep of 7. maxThreshold of 150. this is a magic set
# of parameters for perfect results with target0-target3 data
# sets. it avoids the double-detection of target1's 352 and
# 353 that occurs because 352 was (seemingly) taken as the
# paper tore. however, this is slower and even small tweaks to
# the numbers make it pick up 352 again.

# for now we'll go with approach (a) as it's faster and, more
# importantly, I'm unconvinced that approach (b) is actually
# more accurate in general (different lighting conditions)

class Detector:
    def __init__(self, scoring_context):
        self.scoring_context = scoring_context
        params = cv2.SimpleBlobDetector_Params()
        params.filterByColor = True
        params.blobColor = 255 # white means areas different between frames
        params.minThreshold = 7
        params.thresholdStep = 10
        params.maxThreshold = 147

        self.hole_diameter = scoring_context.hole_diameter * scoring_context.px_per_inch
        hole_radius = self.hole_diameter / 2

        params.filterByArea = True
        # we allow a pretty small min area because we might get shot holes
        # that partially overlap with existing holes
        params.minArea = math.pi * (hole_radius / 4) ** 2
        params.maxArea = math.pi * (hole_radius * 2) ** 2

        # none of these are particularly useful to us given the weird
        # shapes of deltas we can get with overlapping shot holes
        params.filterByCircularity = False
        params.filterByInertia = False
        params.filterByConvexity = False

        # this helps openCV merge neighboring small
        # blobs into bigger, centered blobs
        params.minDistBetweenBlobs = self.hole_diameter

        params.minRepeatability = 2

        ver = (cv2.__version__).split('.')
        if int(ver[0]) < 3:
            self.det = cv2.SimpleBlobDetector(params)
        else:
            self.det = cv2.SimpleBlobDetector_create(params)

    def detect(self, last_image, new_image, known_shots):
        diff = new_image.delta(last_image)

        # we use a slightly larger hole diameter to make sure
        # to cover the hole and torn paper, but no larger than needed,
        # as this will make it harder to detect overlapping shots
        hole_enlarge = 0.01 * self.scoring_context.px_per_inch
        hole_block = int(self.hole_diameter + hole_enlarge)
        for shot in known_shots:
            # black out known shot holes in diff
            # this is to avoid changing light conditions shining through the
            # holes and showing up as a delta, looking like a repeat shot
            diff.draw_circle(shot.pixel_pos, hole_block, 0, -1)

        keypoints = self.det.detect(diff.img)

        if len(keypoints) > 0:
            # biggest hole is the best!
            best = max(keypoints, key=lambda f: f.size)
            x, y = best.pt
            point = (int(round(x)), int(round(y)))
            debugInfo = DebugInfo(last_image, new_image, diff, keypoints)
            return Detection(point, debugInfo, self.hole_diameter)
        else:
            return None

