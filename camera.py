import plat
import time
import image

if plat.debug:
    def init():
        pass
    def dump_samples():
        pass
    def target_image():
        time.sleep(0.25)
        return image.load("samples/target0/7.jpg")
else:
    from picamera import PiCamera
    import scoring

    cam = None

    def init():
        global cam
        if cam != None: return
        cam = PiCamera(sensor_mode = 4) # 1640x1232
        cam.resolution = scoring.source_size
        time.sleep(1)

    def dump_samples():
        init()
        for i in range(0, 60):
            cam.capture("samples/" + str(i) + ".jpg")
            time.sleep(1)

    # capture image from camera
    def target_image():
        init()
        path = plat.imagepath("new.jpg")
        cam.capture(path)
        return image.load(path)

    def raw_image_path():
        init()
        path = plat.imagepath("raw.jpg")
        cam.capture(path)
        return path
