import sqlite3
import constants
import os
import statistics
import scoring
from contextlib import closing
from itertools import groupby

def check_exists(conn, table_name):
    with closing(conn.cursor()) as cursor:
        cursor.execute("SELECT count(*) FROM sqlite_master WHERE type='table' AND name=?", (table_name,))
        row = cursor.fetchone()
        return row[0] > 0

def create_if_not_exists(conn, table_name, table_def):
    if not check_exists(conn, table_name):
        with closing(conn.cursor()) as cursor:
            cursor.execute("CREATE TABLE " + table_name + " " + table_def + ";");
        conn.commit()

def delete_db():
    try:
        os.remove('dbfiles/pitarget.db')
    except OSError:
        pass

def open_db():
    try:
        os.makedirs('dbfiles')
    except FileExistsError:
        # directory already exists
        pass

    conn = sqlite3.connect('dbfiles/pitarget.db')
    conn.row_factory = sqlite3.Row
    if check_exists(conn, 'shots'):
        return conn # shortcut
    create_if_not_exists(conn, 'matches',
                         '''
( match_id integer primary key
, start_time text
, end_time text
, match_name text
, scoring_type text -- scoring context name e.g. "NRA B-40 (10M Air Pistol)"
, practice_mode integer -- 1 or 0
, number_shooters integer
, number_shots integer
)
''')
    create_if_not_exists(conn, 'shooters', '(shooter_id integer primary key, shooter_name text)')
    create_if_not_exists(conn, 'shots',
                         '''
( shot_id integer primary key
, match_id integer references matches(match_id)
, shooter_id integer references shooters(shooter_id)
, shot_time text
, x real -- offset in inches from center, positive is right-of-center, negative left-of-center
, y real -- offset in inches from center, positive is high-of-center, negative low-of-center
, score integer
, xring integer -- 1 or 0
)
''')
    # CREATE INDEX ix_match_listing ON matches(end_time, scoring_type, practice_mode);
    # CREATE INDEX ix_shots_shooter ON shots(shooter_id);
    # CREATE INDEX ix_shots_match ON shots(match_id);
    return conn

def insert_match(conn, match):
    with closing(conn.cursor()) as cursor:
        cursor.execute('INSERT INTO matches(start_time, end_time, match_name, scoring_type, practice_mode, number_shooters, number_shots) VALUES (?, ?, ?, ?, ?, ?, ?);',
                       (match.start_time.isoformat(),
                        match.end_time.isoformat(),
                        match.name,
                        match.scoring_context.name,
                        1 if match.practice_mode else 0,
                        len(match.shooters),
                        match.number_shots))
        return cursor.lastrowid

def get_shooter_id(conn, shooter_name):
    with closing(conn.cursor()) as cursor:
        cursor.execute('SELECT shooter_id FROM shooters WHERE shooter_name = ?', (shooter_name,))
        row = cursor.fetchone()
        id = 0
        if row and row[0]:
            cursor.close()
            id = row[0]
        else:
            cursor.execute('INSERT INTO shooters (shooter_name) values (?); ', (shooter_name,))
            id = cursor.lastrowid
        return id

def shot_offset(shot, dimension):
    pixel_offset = shot.pixel_pos[dimension] - constants.center[dimension]
    inch_offset = float(pixel_offset) / shot.shooter.scoring.px_per_inch
    if dimension == 1:
        # y axis is backwards from how we want to keep it in DB
        inch_offset *= -1.0
    return inch_offset

def save_match(match):
    with closing(open_db()) as conn:
        match_id = insert_match(conn, match)
        for shooter in match.shooters:
            shots = []
            shooter_id = get_shooter_id(conn, shooter.name)
            for shot in shooter.shots:
                shot_time = shot.time.isoformat()
                x = shot_offset(shot, 0)
                y = shot_offset(shot, 1)
                score = shot.score
                xring = 1 if shot.xring else 0
                shots.append((match_id, shooter_id, shot_time, x, y, score, xring))
            with closing(conn.cursor()) as cursor:
                cursor.executemany('INSERT INTO shots (match_id, shooter_id, shot_time, x, y, score, xring) VALUES (?, ?, ?, ?, ?, ?, ?);', shots)
        conn.commit()

def list_shooters():
    with closing(open_db()) as conn:
        with closing(conn.cursor()) as cursor:
            return [ (row[0], row[1]) for row in cursor.execute('SELECT shooter_id, shooter_name FROM shooters') ]

def list_shot_counts():
    with closing(open_db()) as conn:
        with closing(conn.cursor()) as cursor:
            nums = [ row[0] for row in cursor.execute('SELECT DISTINCT number_shots FROM matches') ]
            nums.sort()
            return nums

class MatchSearch:
    def __init__(self, after_time='2000-01-01', before_time='2500-01-01', include_practice=True, include_nonpractice=True, shooter_id=None, match_shot_count=None, scoring_type=None):
        self.after_time = after_time
        self.before_time = before_time
        self.include_practice = include_practice
        self.include_nonpractice = include_nonpractice
        self.shooter_id = shooter_id
        self.match_shot_count = match_shot_count
        self.scoring_type = scoring_type
    def query(self):
        with closing(open_db()) as conn:
            with closing(conn.cursor()) as cursor:
                m = self
                params = [ m.after_time, m.before_time ]
                where = 'm.end_time >= ? AND m.end_time <= ?'
                query = '''
    SELECT *
    FROM matches m
    JOIN (
        SELECT
            s.match_id, sh.shooter_id, sh.shooter_name, sum(s.score) as shooter_score, sum(s.xring) as shooter_xcount
        FROM shots s
        JOIN shooters sh ON sh.shooter_id = s.shooter_id
        GROUP BY s.match_id, sh.shooter_id
    ) sq ON sq.match_id = m.match_id
    WHERE WHERE_EXPR
    ORDER BY m.match_id
    '''
                if not m.include_practice:
                    where += ' AND m.practice_mode = 0'
                if not m.include_nonpractice:
                    where += ' AND m.practice_mode = 1'
                if m.shooter_id != None:
                    where += ' AND EXISTS(SELECT null FROM shots ss WHERE ss.match_id = m.match_id AND ss.shooter_id = ?)'
                    params.append(m.shooter_id)
                if m.match_shot_count != None:
                    where += ' AND m.number_shots = ?'
                    params.append(m.match_shot_count)
                if m.scoring_type != None:
                    where += ' AND m.scoring_type = ?'
                    params.append(m.scoring_type)
                query = query.replace('WHERE_EXPR', where)
                groups = groupby(cursor.execute(query, params), key=lambda r: r['match_id'])
                return [ MatchSearchResult(v) for k, v in groups ]

class MatchSearchResultShooter:
    def __init__(self, row):
        self.name = row['shooter_name']
        self.score = row['shooter_score']
        self.xcount = row['shooter_xcount']

class MatchSearchResult:
    def __init__(self, rows):
        rows = list(rows)
        row = rows[0]
        self.match_id = row['match_id']
        self.start_time = row['start_time']
        self.end_time = row['end_time']
        self.match_name = row['match_name']
        self.scoring_type = row['scoring_type']
        self.practice_mode = row['practice_mode']
        self.number_shooters = row['number_shooters']
        self.number_shots = row['number_shots']
        self.shooters = [ MatchSearchResultShooter(r) for r in rows ]

def trunc_mean(xs, keep_factor):
    middle_len = int(len(xs) * keep_factor)
    throw_out = (len(xs) - middle_len) // 2
    if throw_out == 0:
        return statistics.mean(xs)
    xs.sort()
    return statistics.mean(xs[throw_out:-throw_out])

def compute_zero_offset(scoring_context, shots):
    data_points = [ (s['x'], s['y']) for s in shots ]
    xs = [ s['x'] for s in shots ]
    ys = [ s['y'] for s in shots ]
    potential_offsets = [
        (0.0, 0.0),
        (-statistics.mean(xs), -statistics.mean(ys)),
        (-statistics.median(xs), -statistics.median(ys)),
        (-trunc_mean(xs, 0.9), -trunc_mean(ys, 0.9))
    ]
    potential_offsets = [
        (offset, scoring_context.evaluate_offset_score(offset, data_points)) for offset in potential_offsets
    ]
    best_offset, best_score = max(potential_offsets, key=lambda x: x[1][0] + 0.0001 * x[1][1])
    return best_offset, best_score

def streak(xs, f):
    current_streak = 0
    best_streak = 0
    for x in xs:
        if f(x):
            current_streak += 1
            if current_streak > best_streak:
                best_streak = current_streak
        else:
            current_streak = 0
    return best_streak

class ShooterMatch:
    def __init__(self, match_shots):
        match_shots = list(match_shots)
        r0 = match_shots[0]
        self.match_id = r0['match_id']
        self.match_name = r0['match_name']
        self.match_end_time = r0['match_end_time']
        self.number_shots = len(match_shots)
        self.score = sum(s['score'] for s in match_shots)
        self.xcount = sum(s['xring'] for s in match_shots)

class ShooterStatSearch:
    def __init__(self, shooter_id, after_time='2000-01-01', before_time='2100-01-01', include_practice=True, include_nonpractice=True, match_shot_count=None, scoring_type=None):
        self.shooter_id = shooter_id
        self.after_time = after_time
        self.before_time = before_time
        self.include_practice = include_practice
        self.include_nonpractice = include_nonpractice
        self.match_shot_count = match_shot_count
        self.scoring_type = scoring_type

    def get_shots(self, cursor):
        m = self
        params = [ m.shooter_id, m.after_time, m.before_time ]
        where = 'sh.shooter_id = ? AND m.end_time >= ? AND m.end_time <= ?'
        query = '''
        SELECT
            sh.shooter_id
            , sh.shooter_name
            , m.match_id
            , m.match_name
            , m.end_time as match_end_time
            , s.x
            , s.y
            , s.score
            , s.xring
            , s.shot_time
        FROM shooters sh
        JOIN shots s ON s.shooter_id = sh.shooter_id
        JOIN matches m on m.match_id = s.match_id
        WHERE WHERE_EXPR
        ORDER BY m.match_id, s.shot_id
    '''
        if not m.include_practice:
            where += ' AND m.practice_mode = 0'
        if not m.include_nonpractice:
            where += ' AND m.practice_mode = 1'
        if m.match_shot_count != None:
            where += ' AND m.number_shots = ?'
            params.append(m.match_shot_count)
        if m.scoring_type != None:
            where += ' AND m.scoring_type = ?'
            params.append(m.scoring_type)
        query = query.replace('WHERE_EXPR', where)
        return list(cursor.execute(query, params))

    def query(self):
        with closing(open_db()) as conn:
            with closing(conn.cursor()) as cursor:
                sco = scoring.get_context(self.scoring_type)
                shots = self.get_shots(cursor)
                if len(shots) <= 0:
                    return None
                return ShooterStatResult(sco, shots)

class ShooterStatResult:
    def __init__(self, sco, shots):
        self.shots = shots
        self.shooter_name = shots[0]['shooter_name']
        self.shooter_id = shots[0]['shooter_id']
        self.total_score = (sum(s['score'] for s in shots), sum(s['xring'] for s in shots))
        self.avg_score = statistics.mean(s['score'] for s in shots)
        self.zero_offset, self.offset_score = compute_zero_offset(sco, shots)
        self.offset_avg_score = self.offset_score[0] / float(len(shots))
        self.offset_avg_xcount = self.offset_score[1] / float(len(shots))
        self.ten_streak = streak(shots, lambda x: x['score'] >= 10)
        self.nine_streak = streak(shots, lambda x: x['score'] >= 9)
        self.matches = [ ShooterMatch(group_shots) for _, group_shots in groupby(shots, key=lambda s: s['match_id']) ]
        self.best_match = max((m.score, m.xcount) for m in self.matches)
        self.avg_match = (statistics.mean(m.score for m in self.matches), statistics.mean(m.xcount for m in self.matches))
        self.offset_avg_match_score = (self.offset_avg_score * self.matches[0].number_shots, self.offset_avg_xcount * self.matches[0].number_shots)

    def score_as_of(self, shot_index):
        ss = self.shots[0:shot_index]
        score, xcount = (sum(s['score'] for s in ss), sum(s['xring'] for s in ss))
        return str(score) + "-" + str(xcount) + "X"


class MatchDetails:
    def __init__(self, match_id):
        with closing(open_db()) as conn:
            with closing(conn.cursor()) as cursor:
                query = '''
    SELECT
        sh.shooter_id
        , sh.shooter_name
        , m.match_id
        , m.match_name
        , m.scoring_type
        , m.start_time as match_start_time
        , m.end_time as match_end_time
        , m.number_shots
        , s.x
        , s.y
        , s.score
        , s.xring
        , s.shot_time
    FROM shooters sh
    JOIN shots s ON s.shooter_id = sh.shooter_id
    JOIN matches m on m.match_id = s.match_id
    WHERE m.match_id = ?
    ORDER BY sh.shooter_id, s.shot_id'''
                rows = list(cursor.execute(query, (match_id,)))
                r = rows[0]
                sco = scoring.get_context(r['scoring_type'])
                self.match_id = r['match_id']
                self.match_name = r['match_name']
                self.number_shots = r['number_shots']
                self.start_time = r['match_start_time']
                self.end_time = r['match_end_time']
                self.shooters = [
                    ShooterStatResult(sco, list(sr)) for _, sr in groupby(rows, key=lambda s: s['shooter_id'])
                ]
                self.shooters.sort(reverse=True, key=lambda s: s.total_score)
                self.winner = self.shooters[0]
