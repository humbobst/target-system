import math

def add(a, b):
    x1, y1 = a
    x2, y2 = b
    return (x1 + x2, y1 + y2)

def sub(a, b):
    x1, y1 = a
    x2, y2 = b
    return (x1 - x2, y1 - y2)

def mul(a, b):
    x1, y1 = a
    x2, y2 = b
    return (x1 * x2, y1 * y2)

def div(a, b):
    x1, y1 = a
    x2, y2 = b
    return (x1 / x2, y1 / y2)

def mul_scalar(a, s):
    x1, y1 = a
    return (x1 * s, y1 * s)

def mag(a):
    x1, y1 = a
    return math.sqrt(x1*x1 + y1*y1)

def abs(a, b):
    x1, y1 = a
    return (math.abs(x1), math.abs(y1))

def _avg(x1, x2):
    return (x1 + x2) / 2.0

def avg(a, b):
    x1, y1 = a
    x2, y2 = b
    return (_avg(x1, x2), _avg(y1 - y2))

def norm(a):
    return mul(a, 1.0/mag(a))
