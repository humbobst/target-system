import vec
import json
import image
from constants import *

colors = [
    'red',
    'blue',
    'green',
    'yellow'
]

score_images = {}
for color in colors:
    score_images[color] = [ image.load("gfx/" + str(n) + "_" + color + ".png") for n in range(0, 11) ]
    score_images[color].append(image.load("gfx/x_" + color + ".png"))

unknown_score_image = score_images['red'][0]

def load_bounds(bounds_file):
    with open(bounds_file, 'r') as f:
        s = f.read()
        j = json.loads(s)
        return [
            [j['tlx'], j['tly']],
            [j['trx'], j['try']],
            [j['brx'], j['bry']],
            [j['blx'], j['bly']]
        ]

def save_bounds(bounds, bounds_file):
    j = {
        "tlx": bounds[0][0], "tly": bounds[0][1],
        "trx": bounds[1][0], "try": bounds[1][1],
        "brx": bounds[2][0], "bry": bounds[2][1],
        "blx": bounds[3][0], "bly": bounds[3][1]
    }
    s = json.dumps(j)
    with open(bounds_file, "w") as f:
        f.write(s)


class ScoringContext:
    def __init__(self, name, hole_diameter, rings, actual_width, bounds_file, target_file):
        self.name = name
        self.bounds_file = bounds_file
        self.target_file = target_file
        self.target_img_base = image.load(self.target_file)
        self.rings = rings
        self.hole_diameter = hole_diameter
        self.px_per_inch = scoring_image_size[0] / actual_width
        self.detector = image.Detector(self)
        self.reload_bounds()
        self.recent_debugs = []

    def evaluate_offset_score(self, vec_offset, data_points):
        total_score = 0
        total_xcount = 0
        for inch_vec in data_points:
            inch_vec = vec.add(vec_offset, inch_vec)
            score, xring = self.score_inch(inch_vec)
            total_score += score
            total_xcount += 1 if xring else 0
        return total_score, total_xcount

    def score_inch(self, vec_inches_from_center):
        inches_from_center = vec.mag(vec_inches_from_center)
        hit_diameter = inches_from_center * 2 - self.hole_diameter
        i = 0
        for diameter, score in self.rings:
            if hit_diameter < diameter:
                xring = i + 1 < len(self.rings) and self.rings[i+1][1] == score # if next ring has same numeric score this must be an X
                return score, xring
            i += 1
        return 0, False

    # pixel_pos is on the scoring image (with dimensions scoring_image_size)
    def score(self, pixel_pos):
        diff = vec.sub(pixel_pos, center)
        diff_inches = vec.mul_scalar(diff, 1.0/self.px_per_inch)
        return self.score_inch(diff_inches)

    def reload_bounds(self):
        try:
            self.bounds = load_bounds(self.bounds_file)
        except:
            self.bounds = [[99, 99], [1, 99], [1, 1], [99, 1]]
        self.coeffs = image.find_coeffs(self.bounds, [
            (0, 0),
            (scoring_image_size[0], 0),
            scoring_image_size,
            (0, scoring_image_size[1])
        ])

    def save_new_bounds(self, new_bounds):
        save_bounds(new_bounds, self.bounds_file)
        self.reload_bounds()

    def extract_scoring_image(self, img):
        return img.scoring_filter().perspective_transform(self.coeffs, scoring_image_size)

    # find a shot in the image, if there is one
    def detect_shot(self, last_image, new_image, next_image, known_shots):
        detected1 = self.detector.detect(last_image, new_image, known_shots)
        if detected1 != None:
            # load next image and re-confirm. gotta do this to handle the rare case
            # in which our first image is an action shot with paper incompletely torn (see note 1 below)
            next_image = next_image()
            detected2 = self.detector.detect(last_image, next_image, known_shots)
            if detected2 != None:
                # oh, we got something on both, looks promising...
                diff = vec.sub(detected1.point, detected2.point)
                inches_from_other = vec.mag(diff) / self.px_per_inch
                if inches_from_other < self.hole_diameter * 1.25:
                    # close enough to call it good. we'll believe the 2nd image,
                    # rather than the 1st, because it probably has the cleaner
                    # delta (in the action shot case)
                    self.recent_debugs.append(detected2.debug)
                    if len(self.recent_debugs) > 10:
                        self.recent_debugs.pop(0)
                    detected2.refine(known_shots)
                    return detected2
        return None

    # Note 1: by "action shot", I don't mean we see the pellet in
    # flight. Thus far I've never seen that happen, and think the
    # camera is too slow to pick it up. But I do see cases (like
    # samples/target1/352.jpg) where the paper is half-transparent on
    # the shot hole, then fully clear on the next image captured. I
    # assume this is a blurred image taken as the paper tears.

    def save_overlay_image(self, scoring_image, filename):
        new = self.target_img_base.copy()
        overlay = scoring_image.resize(new.size)
        weighted = new.overlay(overlay)
        weighted.save(filename)

    def save_rings_image(self, scoring_image, filename):
        new = scoring_image.copy()
        scale = self.px_per_inch
        for ring_inches, _ in self.rings:
            new.draw_ring(ring_inches * scale)
        new.save(filename)

    # overlay shots onto the base image and save it to the filename
    def save_target_image(self, old_shots, target_shots, filename):
        if len(old_shots) + len(target_shots) <= 0:
            self.target_img_base.save(filename)
            return

        new = self.target_img_base.copy()
        w, h = new.size
        scale_factor = w / scoring_image_size[0]
        sw, sh = unknown_score_image.size
        sw2, sh2 = sw // 2, sh // 2
        circle_diameter = self.hole_diameter * self.px_per_inch * scale_factor

        def render_shot(shot, style):
            x, y = shot.pixel_pos
            centerx, centery = x * scale_factor, y * scale_factor
            labelx = centerx - sw2
            labely = centery - sh2 + circle_diameter * 1.5
            labelx = max(0, min(w-1, int(labelx)))
            labely = max(0, min(h-1, int(labely)))
            centerx = max(0, min(w-1, int(centerx)))
            centery = max(0, min(h-1, int(centery)))

            if style == 'n':
                new.draw_circle((centerx, centery), circle_diameter, (0, 0, 0))
                new.draw_circle((centerx, centery), circle_diameter - 3, (255, 255, 255), -1)
                shot_image = unknown_score_image
                if shot.score == 10 and shot.xring:
                    shot_image = score_images[shot.shooter.color][11]
                elif shot.score >= 0 and shot.score <= 10:
                    shot_image = score_images[shot.shooter.color][shot.score]
                new.paste(shot_image, (labelx, labely))
            elif style == 'n-1':
                new.draw_circle((centerx, centery), circle_diameter, (0, 0, 0))
                new.draw_circle((centerx, centery), circle_diameter - 3, (150, 150, 150), -1)
            elif style == 't':
                new.draw_circle((centerx, centery), circle_diameter, (0, 0, 0))
                new.draw_circle((centerx, centery), circle_diameter - 3, (50, 50, 50), -1)
            elif style == 'o':
                new.draw_circle((centerx, centery), circle_diameter, (128, 128, 128), 1)
        for shot in old_shots:
            render_shot(shot, 'o')
        for shot in target_shots[0:len(target_shots)-2]:
            render_shot(shot, 't')
        for shot in target_shots[len(target_shots)-2:len(target_shots)-1]:
            render_shot(shot, 'n-1')
        for shot in target_shots[len(target_shots)-1:]:
            render_shot(shot, 'n')

        new.save(filename)

mm = 1/25.4

b40 = ScoringContext("NRA B-40 (10M Air Pistol)", 0.177, [
    # inches in diameter, point value
    # taken from nra-international-pistol-rules.pdf
    (5 * mm, 10), # x ring
    (11.5 * mm, 10),
    (27.5 * mm, 9),
    (43.5 * mm, 8),
    (59.5 * mm, 7),
    (75.5 * mm, 6),
    (91.5 * mm, 5),
    (107.5 * mm, 4),
    (123.5 * mm, 3),
    (139.5 * mm, 2),
    (155.5 * mm, 1)
], 157.5 * mm, "bounds/b-40.json", "gfx/b-40.png")

ar51 = ScoringContext("NRA AR-5/1 (10M Air Rifle)", 0.177, [
    (0.5 * mm, 10),
    (5.5 * mm, 9),
    (10.5 * mm, 8),
    (15.5 * mm, 7),
    (20.5 * mm, 6),
    (25.5 * mm, 5),
    (30.5 * mm, 4),
    (35.5 * mm, 3),
    (40.5 * mm, 2),
    (45.5 * mm, 1)
], 65.5 * mm, "bounds/ar-5.json", "gfx/ar-5.png")

contexts = [ b40, ar51 ]

def get_context(name):
    return next(x for x in contexts if x.name == name)
