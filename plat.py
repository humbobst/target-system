import os

# debugging on windows for dev, deploying on linux
debug = os.name == "nt"

def imagepath(name):
    if debug:
        return "debug/" + name
    else:
        return "/camera/" + name
