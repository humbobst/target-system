from flask import Flask, Blueprint, render_template, send_file, redirect, request
import scoring
import db
from datetime import datetime, timedelta

reporting = Blueprint('reporting', __name__)

@reporting.route("/shooters", methods=['GET'])
def shooters():
    shooters = db.list_shooters()
    shot_counts = db.list_shot_counts()
    if not shooters or not shot_counts:
        return render_template('reporting_empty.html')

    mode = request.args.get('mode')
    mode = 'both' if mode == None else mode
    shot_count = request.args.get('shot_count')
    shot_count = shot_counts[0] if shot_count == None else int(shot_count)
    shooter_id = request.args.get('shooter_id')
    shooter_id = shooters[0][0] if shooter_id == None else int(shooter_id)
    timeframe = request.args.get('timeframe')
    timeframe = 720 if timeframe == None else int(timeframe)

    include_practice = mode != "matches"
    include_nonpractice = mode != "practice"
    search = db.ShooterStatSearch(
        shooter_id,
        include_practice = include_practice,
        include_nonpractice = include_nonpractice,
        after_time = (datetime.utcnow() + timedelta(hours = -timeframe)).isoformat(),
        match_shot_count = shot_count,
        scoring_type = scoring.b40.name)
    results = search.query()
    content = {
        "shot_counts": shot_counts,
        "shooters": shooters,
        "r": results,
        "mode": mode,
        "shot_count": shot_count,
        "shooter_id": shooter_id,
        "timeframe": timeframe,
    }

    return render_template('reporting_shooters.html', **content)

@reporting.route("/matches", methods=['GET'])
def matches():
    shooters = db.list_shooters()
    shot_counts = db.list_shot_counts()
    if not shooters or not shot_counts:
        return render_template('reporting_empty.html')

    mode = request.args.get('mode')
    mode = 'both' if mode == None else mode
    shot_count = request.args.get('shot_count')
    shot_count = shot_counts[0] if shot_count == None else int(shot_count)
    shooter_id = request.args.get('shooter_id')
    shooter_id = shooters[0][0] if shooter_id == None else int(shooter_id)
    timeframe = request.args.get('timeframe')
    timeframe = 720 if timeframe == None else int(timeframe)

    include_practice = mode != "matches"
    include_nonpractice = mode != "practice"
    search = db.MatchSearch(
        shooter_id = shooter_id,
        include_practice = include_practice,
        include_nonpractice = include_nonpractice,
        after_time = (datetime.utcnow() + timedelta(hours = -timeframe)).isoformat(),
        match_shot_count = shot_count,
        scoring_type = scoring.b40.name)
    results = search.query()
    content = {
        "shot_counts": shot_counts,
        "shooters": shooters,
        "r": results,
        "mode": mode,
        "shot_count": shot_count,
        "shooter_id": shooter_id,
        "timeframe": timeframe,
    }

    return render_template('reporting_matches.html', **content)

@reporting.route("/match/<match_id>", methods=['GET'])
def match(match_id):
    details = db.MatchDetails(match_id)
    content = {
        "m": details
    }

    return render_template('reporting_match.html', **content)
