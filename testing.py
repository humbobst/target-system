import os
import shutil
import image
from scoring import *

class TestShot:
    def __init__(self, pixel_pos):
        self.pixel_pos = pixel_pos

def read_hits(dir):
    hits_file = os.path.join(dir, 'hits.txt')
    with open(hits_file, 'r') as f:
        return [ int(l) for l in f.readlines() if l.strip() ]

def test_dir(dir, scoring = b40, saveOutput = False):
    def sample(i):
        return os.path.join(dir, str(i) + ".jpg")

    hits = read_hits(dir)
    jpgs = [ f for f in os.listdir(dir) if f.endswith(".jpg") ]
    nums = [ int(f[0:f.index('.')]) for f in jpgs ]
    nums.sort()

    t1 = None
    t2 = None
    found_hits = []
    shots = []
    dumps = {}
    skips = {}
    for i in nums:
        if i in skips:
            continue
        if t1 == None:
            t1 = image.load(sample(i))
        else:
            t2 = image.load(sample(i))
            def next_image():
                nonlocal t2
                t2 = image.load(sample(i+1))
                skips[sample(i+1)] = True
                return t2
            det = scoring.detect_shot(t1, t2, next_image, shots)
            if det:
                dumps[i] = det.debug
                shots.append(TestShot(det.point))
                found_hits.append(i)
            t1 = t2

    false_negatives = [ h for h in hits if h not in found_hits ]
    false_positives = [ h for h in found_hits if h not in hits ]

    if false_negatives:
        print(dir + " false negatives: " + repr(false_negatives))
        if saveOutput:
            for i in false_negatives:
                folder = os.path.join("debug", dir, str(i))
                os.makedirs(folder)
                shutil.copyfile(sample(i), os.path.join(folder, str(i) + ".jpg"))
                shutil.copyfile(sample(i-1), os.path.join(folder, str(i-1) + ".jpg"))
    if false_positives:
        print(dir + " false positives: " + repr(false_positives))
        if saveOutput:
            for i in false_positives:
                dumps[i].dump(os.path.join("debug", dir, str(i)))

    if not false_negatives and not false_positives:
        print(dir + " perfect!")

if __name__ == '__main__':
    test_dir('samples/target0')
    test_dir('samples/target1')
    test_dir('samples/target2')
    test_dir('samples/target3')
