(function() {
    elements = document.getElementsByClassName('isotime');
    for (var i = 0; i < elements.length; i++) {
        var e = elements[i];
        var t = e.innerText;
        var u = moment.utc(t);
        e.innerText = u.format('YYYY-MM-DD HH:mm');
    }
})();
