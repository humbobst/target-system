from flask import Flask, render_template, send_file, redirect, request
from match import Match
import sys
import datetime
import scoring
import plat
import camera
import db
import reporting

match = Match()

app = Flask(__name__)
app.register_blueprint(reporting.reporting,url_prefix='/reporting')

html_header = """
<html>
<head>
<link type="text/css" rel="stylesheet" href="/static/style.css"/>
</head>
<body>
"""

html_footer = """
<script type="text/javascript">
  setTimeout(function() { window.location.reload() }, 1500);
</script>
</body>
</html>
"""

### MAIN SCREEN

@app.route('/refresher')
def refresher():
    upd = update()
    return html_header + upd + html_footer

@app.route('/')
def live():
    return render_template('live.html')

@app.route('/update')
def update():
    match.update()
    shooter = match.shooter()
    recent_shots = list(enumerate(shooter.shots))[-5:]
    recent_shots.reverse()
    content = {
        "cachebust": match.last_image_update.timestamp(),
        "shooter": shooter,
        "shots_captured": len(shooter.shots),
        "changing_target": match.target_changing,
        "num_shooters": len(match.shooters),
        "recent_shots": recent_shots,
        "practice_mode": match.practice_mode,
    }

    return render_template('update.html', **content)

@app.route('/menu')
def menu():
    return render_template('menu.html')

@app.route('/system_setup')
def system_setup():
    content = {
        "has_debug": len(match.scoring_context.recent_debugs) > 0,
    }
    return render_template('system_setup.html', **content)


### IMAGES

@app.route('/current_raw')
def current_raw():
    return send_file(camera.raw_image_path())

@app.route('/current_target')
def current_target():
    return send_file(match.target_image_filename)

@app.route('/shooter_target')
def shooter_target():
    return send_file(match.shooter().target_shots_filename)

@app.route('/calibration_target')
def calibration_target():
    return send_file(match.calibration_target_path())

### COMMANDS

@app.route('/next_shooter', methods=['POST'])
def next_shooter():
    match.go_next_shooter()
    return redirect('/')

@app.route('/start_change_target', methods=['POST'])
def start_change_target():
    match.start_change_target()
    return redirect('/')

@app.route('/end_change_target', methods=['POST'])
def end_change_target():
    match.end_change_target()
    return redirect('/')

@app.route('/restart_match', methods=['POST'])
def restart_match():
    global match
    match = match.next_match()
    return redirect('/')

@app.route('/delete_stats_db', methods=['POST'])
def delete_stats_db():
    db.delete_db()
    return redirect('/')

@app.route('/sys_exit', methods=['POST'])
def sys_exit():
    sys.exit(0)
    return redirect('/')

@app.route('/clear_last_shot', methods=['POST'])
def clear_last_shot():
    match.clear_last_shot()
    return redirect('/')

@app.route('/new_match', methods=['POST'])
def new_match():
    global match
    num_shooters = int(request.form.get('num_shooters'))
    num_shots = int(request.form.get('num_shots'))
    practice = request.form.get('record_mode') != 'record'
    def default_name(name, i):
        if name: return name
        return 'Shooter ' + str(i)
    names = [ default_name(request.form.get('shooter_name_' + str(i)), i) for i in range(0, num_shooters) ]
    match_name = request.form.get('match_name') or None
    match = Match(names, num_shots, practice_mode=practice, name = match_name)
    return redirect('/')

@app.route('/match_setup')
def match_setup():
    return render_template('match_setup.html')

@app.route('/manual_entry')
def manual_entry():
    cachebust = datetime.datetime.utcnow().timestamp()
    return render_template('manual_entry.html', cb = cachebust)

@app.route('/manual_entry_shot', methods=['POST'])
def manual_entry_shot():
    coords = request.form.get('coords')
    sx, _ = scoring.scoring_image_size
    x, y = [ int(float(a) * sx) for a in coords.split(',') ]
    match.add_fake_shot((x, y))
    return redirect('/')

@app.route('/targetbounds_entry/<i>')
def targetbounds_entry(i):
    i = int(i)
    cachebust = datetime.datetime.utcnow().timestamp()
    labels = ['Top Left', 'Top Right', 'Bottom Right', 'Bottom Left']
    bounds = match.scoring_context.bounds[i]

    return render_template('targetbounds_entry.html', cb = cachebust, index = i, label = labels[i], x = bounds[0], y = bounds[1])

@app.route('/targetbounds/<i>', methods=['POST'])
def targetbounds_set(i):
    i = int(i)
    coords = request.form.get('coords').split(',')
    x = int(scoring.source_size[0] * float(coords[0]))
    y = int(scoring.source_size[1] * float(coords[1]))
    match.scoring_context.bounds[i] = [ x, y ]
    match.scoring_context.save_new_bounds(match.scoring_context.bounds)
    if i >= 3:
        return redirect('/')
    else:
        return redirect('/targetbounds_entry/' + str(i+1))


### DEBUGGING

@app.route('/debug/<i>')
def debug(i):
    i = int(i)
    debug = match.scoring_context.recent_debugs[-i]
    debug.dump(plat.imagepath("debug"))
    nexti = i + 1 if i < len(match.scoring_context.recent_debugs) else False
    previ = i - 1 if i > 1 else False
    cachebust = datetime.datetime.utcnow().timestamp()
    return render_template('debug.html', nexti = nexti, previ = previ, cb = cachebust)

@app.route('/debug_prev')
def debug_prev():
    return send_file(plat.imagepath("debug/prev_image.png"))

@app.route('/debug_new')
def debug_new():
    return send_file(plat.imagepath("debug/new_image.png"))

@app.route('/debug_diff')
def debug_diff():
    return send_file(plat.imagepath("debug/diff_image.png"))

@app.route('/debug_keypoints')
def debug_keypoints():
    return send_file(plat.imagepath("debug/keypoints_image.png"))

### MAIN

if __name__=='__main__':
    app.run(debug=True, use_reloader=False, port=80, host='0.0.0.0')
