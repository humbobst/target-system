from shots import *
import time
import datetime
import camera
import plat
import scoring
import db
import json

samplenum = 0

def save_sample(img):
    global samplenum
    img.save("samples/" + str(samplenum) + ".jpg")
    samplenum += 1

class Match:
    def __init__(self, shooter_names=None, number_shots=5, scoring_context=scoring.b40, practice_mode = True, name = None):
        if shooter_names == None:
            try:
                with open('last_match.json', 'r') as f:
                    s = f.read()
                    j = json.loads(s)
                    shooter_names = j['shooter_names']
                    number_shots = j['number_shots']
                    practice_mode = j['practice_mode']
                    name = j['name']
            except:
                shooter_names = ['Shooter 0']

        self.scoring_context = scoring_context
        self.shooters = [ Shooter(scoring_context, n, number_shots, scoring.colors[i%len(scoring.colors)]) for i, n in enumerate(shooter_names) ]
        self.target_image_filename = plat.imagepath("target.jpg")
        self.target_changed_time = datetime.datetime.utcnow()
        self.target_changing = False
        self.current_shooter = 0
        self.target_was_changing = False
        self.number_shots = number_shots
        self.practice_mode = practice_mode
        self.start_time = datetime.datetime.utcnow()
        self.end_time = None
        self.name = name

        self.set_target_image()
        self.set_all_shooter_images()

        with open('last_match.json', 'w') as f:
            j = {
                "shooter_names": shooter_names,
                "number_shots": number_shots,
                "practice_mode": practice_mode,
                "name": name
            }
            f.write(json.dumps(j))
    def finish_match(self):
        self.end_time = datetime.datetime.utcnow()
        db.save_match(self)
    def is_complete(self):
        return all(len(s.shots) >= self.number_shots for s in self.shooters)
    def go_next_shooter(self):
        self.current_shooter = (self.current_shooter + 1) % len(self.shooters)
    def start_change_target(self):
        self.target_changing = True
    def end_change_target(self):
        self.target_changing = False
    def shooter(self):
        return self.shooters[self.current_shooter]
    def update(self):
        if self.target_changing:
            self.target_was_changing = True
            return
        elif self.target_was_changing:
            self.target_was_changing = False
            self.target_changed_time = datetime.datetime.utcnow()
            self.set_target_image()
            self.set_all_shooter_images()
            self.update()
        else:
            self._target_update()

    def set_target_image(self, new_image = None):
        if new_image == None:
            new_image = self.scoring_context.extract_scoring_image(camera.target_image())
        # save_sample(new_image)
        self.scoring_context.save_overlay_image(new_image, self.target_image_filename)
        self.last_target_image = new_image
        self.last_image_update = datetime.datetime.utcnow()

    def set_all_shooter_images(self):
        for shooter in self.shooters:
            self.set_shooter_images(shooter, self.last_target_image)

    def set_shooter_images(self, shooter, new_image):
        latest_shot = []
        if len(shooter.shots) > 0 and shooter.shots[-1].time > self.target_changed_time:
            latest_shot.append(shooter.shots[-1])
        old_shots = [ s for s in shooter.shots if s.time <= self.target_changed_time ]
        target_shots = [ s for s in shooter.shots if s.time > self.target_changed_time ]
        self.scoring_context.save_target_image(old_shots, target_shots, shooter.target_shots_filename)
        self.last_image_update = datetime.datetime.utcnow()
        if self.is_complete():
            self.finish_match()

    def add_shot(self, shot, new_image):
        shooter = self.shooters[self.current_shooter]
        shooter.add_shot(shot)
        self.set_shooter_images(shooter, new_image)

    def _target_update(self):
        new_image = None
        def next_image():
            nonlocal new_image
            new_image = self.scoring_context.extract_scoring_image(camera.target_image())
            return new_image
        next_image()
        target_shot_holes = [
            shot
            for shooter in self.shooters
            for shot in shooter.shots
            if shot.time > self.target_changed_time
        ]
        detected = self.scoring_context.detect_shot(self.last_target_image, new_image, next_image, target_shot_holes)
        if detected != None:
            pixel = detected.point
            if self.is_complete():
                # auto-reset when all shooters are done and a new shot is detected
                self.reset()
            self.add_shot(pixel, new_image)
        self.set_target_image(new_image)

    def next_match(self):
        return Match([ s.name for s in self.shooters ], self.number_shots, self.scoring_context, self.practice_mode, self.name)

    def reset(self):
        self.__init__([ s.name for s in self.shooters ], self.number_shots, self.scoring_context, self.practice_mode, self.name)

    def clear_last_shot(self):
        shooter = self.shooters[self.current_shooter]
        shooter.shots.pop()
        self.set_shooter_images(shooter, self.last_target_image)

    def add_fake_shot(self, shot):
        if self.is_complete():
            # auto-reset when all shooters are done and a new shot is detected
            self.reset()
        shooter = self.shooters[self.current_shooter]
        shooter.add_shot(shot)
        self.set_shooter_images(shooter, self.last_target_image)

    def calibration_target_path(self):
        path = plat.imagepath("calibration.jpg")
        self.scoring_context.save_rings_image(self.last_target_image, path)
        return path
