import datetime
import plat

class Shot:
    def __init__(self, shooter, pixel_pos):
        self.shooter = shooter
        self.time = datetime.datetime.utcnow()
        self.pixel_pos = pixel_pos
        self.score, self.xring = shooter.scoring.score(pixel_pos)

class Shooter:
    def __init__(self, scoring, name, number_shots, color = "#ff0000"):
        self.name = name
        self.scoring = scoring
        self.color = color
        self.shots = []
        self.number_shots = number_shots
        self.target_shots_filename = plat.imagepath(name + "_target.jpg")
    def add_shot(self, shot_px):
        self.shots.append(Shot(self, shot_px))
    def latest_score(self):
        if len(self.shots) <= 0: return 0
        return self.shots[-1].score
    def current_score(self):
        return sum((shot.score for shot in self.shots))
    def current_xcount(self):
        return sum(1 for shot in self.shots if shot.xring)
    def projected_score(self):
        if len(self.shots) <= 0: return 0
        score_so_far = self.current_score()
        avg_per_shot = score_so_far / len(self.shots)
        return avg_per_shot * self.number_shots

