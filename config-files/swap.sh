#!/bin/sh

if [ "$1" = "on" ]; then
    systemctl stop dhcpcd
    systemctl stop dnsmasq
    systemctl stop hostapd
    systemctl stop wpa_supplicant
    
    wpa_cli terminate || true
    cp -r /home/pi/target-system/config-files/ap/* /etc/

    systemctl unmask hostapd
    systemctl enable hostapd
else
    systemctl stop dhcpcd
    systemctl stop dnsmasq
    systemctl stop hostapd
    systemctl stop wpa_supplicant

    systemctl disable hostapd
    systemctl mask hostapd
    
    cp -r /home/pi/target-system/config-files/original/* /etc/
fi

