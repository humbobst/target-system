from pyzbar import pyzbar
import cv2
from picamera import PiCamera
import time
import os

def replace(filename, d):
    with open(filename, 'r') as f:
        text = f.read()
    for k, v in d.items():
        text = text.replace(k, v)
    with open(filename, 'w') as f:
        f.write(text)

def enable_ap(ssid, psk):
    os.system("./swap.sh on")
    replace("/etc/hostapd/hostapd.conf", {
        "{{SSID}}": ssid,
        "{{PSK}}": psk
    })
    os.system("reboot")

def enable_wifi(ssid, psk):
    os.system("./swap.sh off")
    replace("/etc/wpa_supplicant/wpa_supplicant.conf", {
        "{{SSID}}": ssid,
        "{{PSK}}": psk
    })
    os.system("reboot")

def save(data):
    with open("../debug/wifi.txt", 'w') as f:
        f.write(data)

def load_current():
    try:
        with open("../debug/wifi.txt", 'r') as f:
            return f.read()
    except:
        return ""

def execute_barcode(data):
    if data.strip() == load_current():
        return
    else:
        save(data)
    lines = data.splitlines()
    if len(lines) < 3:
        return
    if not lines[0].startswith('PT.'): # PiTarget identifier
        return
    enable = None
    if '.wf' in lines[0]:
        enable = enable_wifi
    elif '.ap' in lines[0]:
        enable = enable_ap
    else:
        return
    ssid = lines[1].strip()
    psk = lines[2].strip()
    enable(ssid, psk)

def main():
    cam = PiCamera(sensor_mode = 4)
    time.sleep(1)
    path =  "/camera/qr.jpg"
    for _ in range(0, 3):
        cam.capture(path)
        start = time.perf_counter()
        image = cv2.imread(path)
        barcodes = pyzbar.decode(image)
        elapsed = time.perf_counter() - start
        print("Found " + str(len(barcodes)) + " barcodes")
        for barcode in barcodes:
            data = barcode.data.decode('utf-8')
            execute_barcode(data)

if __name__ == '__main__':
    main()
