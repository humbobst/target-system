#!/bin/sh

while true; do
    ID=`cat debug/id.txt`
    IP=`ifconfig wlan0 | grep -o 'inet [0-9]\+\.[0-9]\+\.[0-9]\+\.[0-9]\+' | sed 's/inet //'`
    curl -d "id=$ID&ip=$IP" 'https://peelewoodworking.com/Home/ReportTarget'
    sleep 60
done
